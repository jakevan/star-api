package api.faction;

import api.common.GameServer;
import api.entity.StarPlayer;
import api.entity.StarStation;
import api.mod.StarLoader;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StarFaction {

    private org.schema.game.common.data.player.faction.Faction internalFaction;

    public static StarFaction fromId(int id){
        if(id == 0){
            return null;
        } else {
            return new StarFaction(StarLoader.getGameState().getFactionManager().getFaction(id));
        }
    }

    public StarFaction(org.schema.game.common.data.player.faction.Faction internalFaction) {
        this.internalFaction = internalFaction;
    }

    public org.schema.game.common.data.player.faction.Faction getInternalFaction() {
        return internalFaction;
    }

    public int getID() {
        return internalFaction.getIdFaction();
    }

    public String getName() {
        return internalFaction.getName();
    }

    public void setName(String name) {
        internalFaction.setName(name);
    }

    public List<StarPlayer> getMembers() {
        List<StarPlayer> members = new ArrayList<StarPlayer>();
        GameServerState gameServerState = GameServerState.instance;
        for(String uid : internalFaction.getMembersUID().keySet()) {
            StarPlayer player = new StarPlayer(getPlayerStateFromUID(uid));
            members.add(player);
        }
        return members;
    }

    public void addMember(StarPlayer player) {
        player.getPlayerState().getFactionController().forceJoinOnServer(internalFaction.getIdFaction());
    }
    public FactionRelation.RType getRelationTo(StarFaction other){
        return internalFaction.getRelationshipWithFactionOrPlayer(other.getID());
    }
    public void removeMember(StarPlayer player) {
        player.getPlayerState().getFactionController().leaveFaction();
    }

    public List<StarPlayer> getActiveMembers() {
        List<StarPlayer> activeMembers = new ArrayList<StarPlayer>();
        for(StarPlayer player : getMembers()) {
            if(internalFaction.getMembersUID().get(player.getPlayerState().getUniqueIdentifier()).isActiveMember()) {
                activeMembers.add(player);
            }
        }
        return activeMembers;
    }

    public List<StarPlayer> getInactiveMembers() {
        List<StarPlayer> inactiveMembers = new ArrayList<StarPlayer>();
        for(StarPlayer player : getMembers()) {
            if(!internalFaction.getMembersUID().get(player.getPlayerState().getUniqueIdentifier()).isActiveMember()) {
                inactiveMembers.add(player);
            }
        }
        return inactiveMembers;
    }

    public List<StarFaction> getAllies() {
        List<StarFaction> allies = null;
        for(org.schema.game.common.data.player.faction.Faction internalAlly : internalFaction.getFriends()) {
            StarFaction faction = new StarFaction(internalAlly);
            allies.add(faction);
        }
        return allies;
    }

    public List<StarFaction> getEnemies() {
        List<StarFaction> enemies = new ArrayList<StarFaction>();
        for(org.schema.game.common.data.player.faction.Faction internalEnemy : internalFaction.getEnemies()) {
            StarFaction faction = new StarFaction(internalEnemy);
            enemies.add(faction);
        }
        return enemies;
    }

    public void setAlly(StarFaction faction) {
        internalFaction.getFriends().add(faction.internalFaction);
    }

    public void setEnemy(StarFaction faction) {
        internalFaction.getEnemies().add(faction.internalFaction);
    }

    public List<StarPlayer> getPersonalEnemies() {
        List<StarPlayer> personalEnemies = new ArrayList<StarPlayer>();
        for(String uid : internalFaction.getPersonalEnemies()) {
            personalEnemies.add(new StarPlayer(getPlayerStateFromUID(uid)));
        }

        return personalEnemies;
    }

    public StarStation getHomebase() throws IOException {
        StarStation homebase = null;
        Vector3i internalHomeCoords = internalFaction.getHomeSector();
        Sector internalSector = GameServer.getServerState().getUniverse().getSector(internalHomeCoords, true);
        for(SimpleTransformableSendableObject internalEntity : internalSector.getEntities()) {
            if(internalEntity.isSegmentController() && internalEntity.getType() == SimpleTransformableSendableObject.EntityType.SPACE_STATION) {
                homebase = new StarStation((SegmentController) internalEntity);
            }
        }
        return homebase;
    }

    private PlayerState getPlayerStateFromName(String playerName) {
        GameServerState gameServerState = GameServerState.instance;
        Map<String, PlayerState> playerStates = gameServerState.getPlayerStatesByName();
        PlayerState pState = null;
        try {
            pState = playerStates.get(playerName);
        } catch(Exception e) {
            System.err.println("[StarLoader API]: Tried to get a PlayerState from name, but specified player was not found on server!");
            e.printStackTrace();
        }
        return pState;
    }

    private PlayerState getPlayerStateFromUID(String UID) {
        GameServerState gameServerState = GameServerState.instance;
        Map<String, PlayerState> playerStates = gameServerState.getPlayerStatesByName();
        PlayerState pState = null;
        for(PlayerState playerState : playerStates.values()) {
            if(playerState.getUniqueIdentifier().equals(UID)) {
                pState = playerState;
            }
        }
        return pState;
    }
}
