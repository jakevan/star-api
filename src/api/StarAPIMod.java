package api;

import api.mod.StarMod;

public class StarAPIMod extends StarMod {
    public static void main(String[] args) {

    }
    public static StarAPIMod inst;
    @Override
    public void onGameStart() {
        inst = this;
        setModName("StarAPI").setModVersion("1.6");
        setModDescription("A mod containing many wrappers and systems to make modding easier to approach");
    }
}
