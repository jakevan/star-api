package api.server;

import api.common.GameClient;
import api.entity.StarEntity;
import api.entity.StarPlayer;
import api.entity.Ship;
import api.universe.StarUniverse;
import com.bulletphysics.linearmath.Transform;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.controller.manager.ingame.PlayerInteractionControlManager;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.fleet.Fleet;
import org.schema.game.common.data.physics.PhysicsExt;
import org.schema.game.common.data.physics.Vector3fb;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;

import javax.vecmath.Vector3f;
import java.util.ArrayList;
import java.util.Collection;

import static api.common.GameClient.getCurrentControl;

public class ClientUtils {
    public static StarEntity getSelectedEntity(){

        int selectedEntityId = GameClient.getClientPlayerState().getSelectedEntityId();
        return StarUniverse.getUniverse().getEntityFromId(selectedEntityId);
    }
    public static void selectEntity(StarEntity e){
        getControlManager().setSelectedEntity(e.internalEntity);
    }
    public static PlayerInteractionControlManager getControlManager(){
        return GameClient.getClientState().getGlobalGameControlManager().getIngameControlManager().getPlayerGameControlManager().getPlayerIntercationManager();
    }
    public static Vector3i getWaypoint(){
        return GameClient.getClientController().getClientGameData().getWaypoint();
    }
    public static void setWaypoint(Vector3i v){
        GameClient.getClientController().getClientGameData().setWaypoint(v);
    }
    public static ArrayList<StarEntity> getNearbyEntities(){
        ArrayList<StarEntity> entities = new ArrayList<StarEntity>();
        for (SimpleTransformableSendableObject<?> value : GameClient.getClientState().getCurrentSectorEntities().values()) {
            if(value instanceof SegmentController) {
                entities.add(new StarEntity((SegmentController) value));
            }
        }
        return entities;
    }
    public static Ship getCurrentShip(){
        PlayerControllable con = getCurrentControl();
        if(con instanceof Ship){
            return (Ship) con;
        }else{
            return null;
        }
    }
    public static StarEntity getCurrentEntity(){
        PlayerControllable con = getCurrentControl();
        if(con instanceof SegmentController){
            return new StarEntity((SegmentController) con);
        }else{
            return null;
        }
    }
    public static Collection<Fleet> getAvailableFleets(){
        return GameClient.getClientState().getFleetManager().getAvailableFleetsClient();
    }

    public static void spawnBlockParticle(short id, Vector3f pos){
        GameClientState state = GameClient.getClientState();
        if(state == null){
            return;
        }
        final Vector3fb vector3fb = new Vector3fb(pos);
        final Transform transform = new Transform();
        transform.setIdentity();
        transform.origin.set(vector3fb);
        state.getWorldDrawer().getShards().voronoiBBShatter((PhysicsExt)state.getPhysics(), transform, id, state.getCurrentSectorId(), transform.origin, null);
    }

    public static StarPlayer getPlayer() {
        return new StarPlayer(GameClient.getClientPlayerState());
    }
}
