package api.universe;

import api.DebugFile;
import api.common.GameServer;
import api.entity.StarEntity;
import api.mod.StarLoader;
import api.server.Server;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.world.Sector;
import org.schema.schine.network.objects.Sendable;

import java.io.IOException;

public class StarUniverse {

    private Server server;

    public StarUniverse() {

    }

    public Server getServer() {
        return server;

    }
    /*public SegmentController spawnSegmentController(Sector sector, String catalogName, String name, int factionId){
        BluePrintController blueprintController = BluePrintController.active;
        Transform transform = new Transform();
        transform.setIdentity();
        final SegmentControllerOutline<?> loadBluePrint;
        try {
            loadBluePrint = blueprintController.loadBluePrint(GameServer.getServerState(), catalogName, name, transform, -1, factionId, blueprintController.readBluePrints(), sector.pos, null, "<system>", Sector.buffer, true, null, new ChildStats(false));
            loadBluePrint.spawnSectorId = new Vector3i(sector.pos);
        } catch (EntityNotFountException | IOException | EntityAlreadyExistsException e) {
            e.printStackTrace();
        }
        GameServerController
    }*/

    public StarSector getSector(int x, int y, int z) {
        /**
         * Gets a sector using it's coordinates.
         */
        return getSector(new Vector3i(x,y,z));
    }
    /**
     * Gets a sector using it's vector coordinates.
     */
    public StarSector getSector(Vector3i sectorCoords) {
        return getSector(sectorCoords, true);
    }
    public StarSector getSector(Vector3i sectorCoords, boolean load) {
        try {
            if(load) {
                return new StarSector(GameServer.getServerState().getUniverse().getSector(sectorCoords));
            } else {
                return new StarSector(GameServer.getServerState().getUniverse().getSectorWithoutLoading(sectorCoords));
            }
        } catch (IOException e) {
            DebugFile.log("[ERROR] Getting sector returned error");
            e.printStackTrace();
        }
        return null;
    }

    public StarStellarSystem getSystem(Vector3i systemCoords) {
        /**
         * Gets a system using it's vector coordinates.
         */
        try {
            return new StarStellarSystem(GameServer.getServerState().getUniverse().getStellarSystemFromStellarPos(systemCoords));
        } catch (IOException e) {
            DebugFile.log("[ERROR] Getting system returned error");
            e.printStackTrace();
        }
        return null;
    }

    public StarStellarSystem getSystem(int x, int y, int z) {
        /**
         * Gets a system using it's coordinates.
         */
        Vector3i systemCoords = new Vector3i();
        systemCoords.x = x;
        systemCoords.y = y;
        systemCoords.z = z;

        try {
            return new StarStellarSystem(GameServer.getServerState().getUniverse().getStellarSystemFromStellarPos(systemCoords));
        } catch (IOException e) {
            DebugFile.log("[ERROR] Getting system returned error");
            e.printStackTrace();
        }
        return null;
    }
    public StarEntity getEntityFromId(int id){
        Sendable sendable = StarLoader.getGameState().getState().getLocalAndRemoteObjectContainer().getLocalObjects().get(id);
        if(sendable instanceof ManagedSegmentController){
            return new StarEntity((SegmentController) sendable);
        }
        return null;// TODO this
        //Universe.getUniverse().getSector()
    }

    private static StarUniverse universe;

    public static StarUniverse getUniverse() {
        /**
         * Gets the server universe.
         */
        if(universe == null) universe = new StarUniverse();
        return universe;
    }
}
