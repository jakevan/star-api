package api.universe;

import api.common.GameServer;
import api.entity.StarEntity;
import api.faction.StarFaction;
import api.mod.StarLoader;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.server.data.GameServerState;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public class StarStellarSystem {

    private StellarSystem internalSystem;

    public StarStellarSystem(StellarSystem internalSystem) {
        this.internalSystem = internalSystem;
    }

    public ArrayList<StarSector> getSectors() {
        /**
         * Gets all the sectors inside the system. Currently doesn't work.
         */
        ArrayList<StarSector> sectors = new ArrayList<StarSector>();
        //((VoidSystem) internalSystem).getSunSectorPosAbs(Galaxy.ge)

        //Todo:Figure out how to get all sectors in a system.
        return sectors;
    }
    public StarFaction getOwnerFaction(){
        org.schema.game.common.data.player.faction.Faction internalFaction = StarLoader.getGameState().getFactionManager().getFaction(internalSystem.getOwnerFaction());
        if(internalFaction == null){
            return null;
        }
        return new StarFaction(internalFaction);
    }

    public StellarSystem getInternalSystem() {
        return internalSystem;
    }

    public StarFaction getFaction(){
        return StarFaction.fromId(internalSystem.getOwnerFaction());
    }
    public String getOwnerUID(){
        return internalSystem.getOwnerUID();
    }
    public Vector3i getOwnerPos(){
        return internalSystem.getOwnerPos();
    }
    public void resetClaim(){
        internalSystem.setOwnerFaction(0);
    }
    public boolean isClaimed(){
        StarFaction ownerFaction = getOwnerFaction();
        if(ownerFaction == null){
            return false;
        }
        return ownerFaction.getID() != 0;
    }
    public void claim(StarEntity e){
        internalSystem.setOwnerUID(e.getUID());
        internalSystem.setOwnerFaction(e.getFaction().getID());
        internalSystem.getOwnerPos().set(e.getSectorPosition());
        GameServerState server = GameServer.getServerState();
        try {
            server.getDatabaseIndex().getTableManager().getSystemTable().updateOrInsertSystemIfChanged(internalSystem, true);

            server.getUniverse()
                    .getGalaxyFromSystemPos(internalSystem.getPos()).getNpcFactionManager()
                    .onSystemOwnershipChanged(0, internalSystem.getOwnerFaction(), internalSystem.getPos());
            StarLoader.getGameState().sendGalaxyModToClients(internalSystem, e.getSectorPosition());

        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
