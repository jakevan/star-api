package api.systems;

import org.schema.game.common.controller.elements.power.reactor.tree.ReactorElement;
import java.util.List;

public class StarChamber {

    private ReactorElement internalChamber;

    public StarChamber(ReactorElement internalChamber) {
        this.internalChamber = internalChamber;
    }

    public List<StarChamber> getChildren() {
        List<StarChamber> children = null;
        for(ReactorElement internalChildChamber : internalChamber.children) {
            children.add(new StarChamber(internalChildChamber));
        }
        return children;
    }

    public int getSize() {
        return internalChamber.getSize();
    }

    public boolean isDamaged() {
        return internalChamber.isDamaged();
    }

    public boolean hasValidConduit() {
        return internalChamber.validConduit;
    }

    public boolean isRoot() {
        return internalChamber.isRoot();
    }
}
