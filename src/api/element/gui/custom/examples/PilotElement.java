package api.element.gui.custom.examples;

import api.common.GameClient;
import api.element.gui.custom.CustomHudText;
import api.entity.StarEntity;
import api.entity.StarPlayer;
import org.newdawn.slick.Color;
import org.newdawn.slick.UnicodeFont;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.schine.input.InputState;

import javax.vecmath.Vector4f;
import java.util.ArrayList;

public class PilotElement extends CustomHudText {
    public PilotElement(UnicodeFont unicodeFont, InputState inputState) {
        super(100, 20, unicodeFont, Color.green, inputState);
    }

    public StarEntity entity;
    public void setEntity(StarEntity e){
        entity = e;
    }

    @Override
    public void onInit() {
        super.onInit();
        setTextSimple(new Object(){
            @Override
            public String toString() {
                if(entity == null) return "";
                ArrayList<StarPlayer> attachedPlayers = entity.getAttachedPlayers();
                if(attachedPlayers.size() > 0){
                    StarPlayer player = attachedPlayers.get(0);
                    FactionRelation.RType relation = GameClient.getClientState().getPlayer().getRelation(player.getPlayerState());
                    PilotElement.this.setColor(new Vector4f(relation.defaultColor.x, relation.defaultColor.y, relation.defaultColor.z, 255));
                    return player.getName();
                }else{
                    return "No Pilot";
                }
            }
        });
    }
}
