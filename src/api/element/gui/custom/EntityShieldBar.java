package api.element.gui.custom;

import api.entity.StarEntity;
import api.systems.StarShield;
import org.schema.common.util.StringTools;

public abstract class EntityShieldBar extends CustomHudBar {

    public StarEntity entity;
    public void setEntity(StarEntity e){
        entity = e;
    }

    @Override
    public boolean drawBar() {
        return entity != null;
    }



    @Override
    public float getFilled() {
        if(entity == null){
            return 0F;
        }
        StarShield lastHitShield = entity.getLastHitShield();
        if(lastHitShield == null){
            return 0F;
        }
        return lastHitShield.getPercent();
    }

    @Override
    public String getText() {
        if(entity == null){
            return "N/A";
        }
        StarShield lastHitShield = entity.getLastHitShield();
        if(lastHitShield == null){
            return "Shields: N/A";
        }
        return "Shields: [" + StringTools.massFormat(lastHitShield.getCurrentShields())
                + " / " + StringTools.massFormat(lastHitShield.getMaxCapacity()) + "]";
    }
}
