package api.element.gui.custom;

import api.element.gui.elements.GUIElement;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.input.InputState;

/**
 * Created by Jake on 12/15/2020.
 * <insert description here>
 */
public class CustomHudImage extends GUIElement {
    private Sprite sprite;

    public CustomHudImage(InputState inputState, int width, int height, Sprite sprite) {
        super(inputState, width, height);
        this.sprite = sprite;
    }

    @Override
    public void draw() {
        ShaderLibrary.scanlineShader.load();
        sprite.draw();
        ShaderLibrary.scanlineShader.unload();
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

}
