package api.element.block;

import org.schema.game.common.data.element.FactoryResource;

import java.util.ArrayList;
import java.util.Arrays;

public class Recipe {

    private ArrayList<FactoryResource> recipe;
    private int bakeTime;
    private FactoryType factoryType;

    public Recipe(FactoryType factoryType) {
        this(factoryType, 100);
    }

    public Recipe(FactoryType factoryType, int bakeTime, FactoryResource... resources) {
        this.bakeTime = bakeTime;
        this.recipe = new ArrayList<>();
        this.factoryType = factoryType;
        if(resources != null) this.recipe.addAll(Arrays.asList(resources));
    }

    public FactoryResource[] getRecipe() {
        return (FactoryResource[]) recipe.toArray();
    }

    public int getBakeTime() {
        return bakeTime;
    }

    public void setBakeTime(int bakeTime) {
        this.bakeTime = bakeTime;
    }

    public void addResource(short id, int count) {
        recipe.add(new FactoryResource(count, id));
    }

    public void removeResource(short id) {
        recipe.remove(id);
    }

    public FactoryType getFactoryType() {
        return factoryType;
    }

    public void setFactoryType(FactoryType factoryType) {
        this.factoryType = factoryType;
    }
}
