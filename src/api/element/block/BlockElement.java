package api.element.block;

import org.schema.game.common.data.element.ElementInformation;

public abstract class BlockElement {

    private ElementInformation blockInfo;
    private String name;
    private String description;
    private int price;
    private Recipe recipe;
    private boolean canActivate;
    private boolean door;
    private short[] textureIDs;

    public BlockElement(String name) {
        this.name = name;
        this.description = "";
        this.price = 0;
        this.recipe = null;
        this.canActivate = false;
        this.door = false;
        this.textureIDs = new short[] {0, 0, 0, 0, 0, 0};
    }

    public ElementInformation getBlockInfo() {
        return blockInfo;
    }

    public void setBlockInfo(ElementInformation blockInfo) {
        this.blockInfo = blockInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public boolean isCanActivate() {
        return canActivate;
    }

    public void setCanActivate(boolean canActivate) {
        this.canActivate = canActivate;
    }

    public boolean isDoor() {
        return door;
    }

    public void setDoor(boolean door) {
        this.door = door;
    }

    public short[] getTextureIDs() {
        return textureIDs;
    }

    public void setTextureIDs(short[] textureIDs) {
        this.textureIDs = textureIDs;
    }

    public void onActivation() {

    }
}
