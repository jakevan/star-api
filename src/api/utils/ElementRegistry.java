package api.utils;

import api.config.BlockConfig;
import api.element.block.BlockElement;
import org.schema.game.common.data.element.ElementInformation;

public class ElementRegistry {

    public static void registerElement(BlockElement block) {
        ElementInformation blockInfo = BlockConfig.newElement(block.getName(), block.getTextureIDs());
        blockInfo.setTextureId(block.getTextureIDs());
        blockInfo.setFullName(block.getName());
        blockInfo.setDescription(block.getDescription());
        if(block.getPrice() == 0) {
            blockInfo.setShoppable(false);
        } else {
            blockInfo.setShoppable(true);
            blockInfo.setPrice(block.getPrice());
        }
        if(block.getRecipe() == null) {
            blockInfo.setInRecipe(false);
        } else {
            blockInfo.setInRecipe(true);
            BlockConfig.addRecipe(blockInfo, block.getRecipe().getFactoryType().getId(), block.getRecipe().getBakeTime(), block.getRecipe().getRecipe());
        }
        blockInfo.setCanActivate(block.isCanActivate());
        blockInfo.setDoor(block.isDoor());
        block.setBlockInfo(blockInfo);
    }
}
